Source: geonames
Section: gnome
Priority: optional
Maintainer: Debian UBports Team <team+ubports@tracker.debian.org>
Uploaders: Mike Gabriel <sunweaver@debian.org>,
           Marius Gripsgard <marius@ubports.com>,
Build-Depends: cmake,
               debhelper-compat (= 13),
               gtk-doc-tools (>= 1.21),
               intltool,
               libglib2.0-dev,
               locales,
               locales-all,
               pkg-config,
               rdfind,
               symlinks,
Standards-Version: 4.6.2
Rules-Requires-Root: no
Homepage: https://gitlab.com/ubports/development/core/geonames
Vcs-Git: https://salsa.debian.org/ubports-team/geonames.git
Vcs-Browser: https://salsa.debian.org/ubports-team/geonames/

Package: libgeonames0
Architecture: any
Multi-Arch: same
Section: libs
Depends: libgeonames-common (= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: Parse and query the geonames database dump
 A library for parsing and querying a local copy of the geonames.org database.
 .
 This package contains the shared libraries.

Package: libgeonames-common
Architecture: all
Multi-Arch: foreign
Section: libs
Depends: ${misc:Depends},
Description: geonames - data files
 A library for parsing and querying a local copy of the geonames.org database.
 .
 This package contains library data files.

Package: libgeonames-dev
Architecture: any
Section: libdevel
Multi-Arch: same
Depends: libgeonames0 (= ${binary:Version}),
         libglib2.0-dev,
         ${misc:Depends},
Description: geonames - library development files
 A library for parsing and querying a local copy of the geonames.org database.
 .
 This package contains the header and development files which are needed to use
 the libgeonames library.
